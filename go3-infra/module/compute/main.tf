########################
## Auto Scaling Group ##
########################
resource "aws_autoscaling_group" "this" {
  count                 = var.create_asg ? 1 : 0
  name_prefix           = var.name_prefix
  vpc_zone_identifier   = var.vpc_zone_identifier
  desired_capacity      = var.desired_capacity
  max_size              = var.max_size
  min_size              = var.min_size
  health_check_type     = var.health_check_type
  health_check_grace_period   = var.health_check_grace_period
  force_delete                = var.force_delete
  launch_template {
    #id                  = var.launch_template_id
    name                = var.launch_template_name
    version             = var.launch_template_version
  }

  #################
  ## ASG Metrics ##
  #################
  metrics_granularity   = var.metrics_granularity
  target_group_arns     = var.target_group_arns
}

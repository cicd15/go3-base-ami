variable "region" {
  type        = string
  description = "Whether to create autoscaling group"
  default     = "eu-west-1"
}

variable "create_asg" {
  type        = bool
  description = "Whether to create autoscaling group"
  default     = true
}

variable "name_prefix" {
  type        = string
  description = "Creates a unique name beginning with the specified prefix"
  default     = ""
}

variable "vpc_zone_identifier" {
  type        = list(string)
  description = "A list of subnet IDs to launch resources in"
  default     = [""]
}

variable "desired_capacity" {
  type        = string
  description = "The number of Amazon EC2 instances that should be running in the group"
  default     = "1"
}

variable "max_size" {
  type        = string
  description = "The maximum size of the auto scale group"
  default     = "1"
}

variable "min_size" {
  type        = string
  description = "The minimum size of the auto scale group"
  default     = "1"
}

variable "health_check_grace_period" {
  type        = string
  description = "Time (in seconds) after instance comes into service before checking health"
  default     = "300"
}

variable "health_check_type" {
  type        = string
  description = "Controls how health checking is done. Values are - EC2 and ELB"
  default     = "EC2"
}

variable "force_delete" {
  type        = bool
  description = "Allows deleting the autoscaling group without waiting for all instances in the pool to terminate. You can force an autoscaling group to delete even if it's in the process of scaling a resource. Normally, Terraform drains all the instances before deleting the group. This bypasses that behavior and potentially leaves resources dangling"
  default     = false
}

//variable "launch_template_id" {
//  type        = string
//  description = "The ID of the launch template. Conflicts with name"
//  default     = ""
//}

variable "launch_template_name" {
  type        = string
  description = "The ID of the launch template. Conflicts with name"
  default     = "default"
}

variable "launch_template_version" {
  type        = string
  description = "Template version"
  default     = "$Latest"
}

variable "metrics_granularity" {
  type        = string
  description = "The granularity to associate with the metrics to collect. The only valid value is 1Minute"
  default     = "1Minute"
}

variable "target_group_arns" {
  description = "A list of aws_alb_target_group ARNs, for use with Application Load Balancing"
  type        = list(string)
  default     = []
}

variable "enabled_metrics" {
  type        = list
  description = "A list of metrics to collect. The allowed values are GroupMinSize, GroupMaxSize, GroupDesiredCapacity, GroupInServiceInstances, GroupPendingInstances, GroupStandbyInstances, GroupTerminatingInstances, GroupTotalInstances"
  default     = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]
}
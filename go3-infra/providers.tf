# Configure the AWS Provider
provider "aws" {
  version = "~> 2.68.0"
  region  = "eu-west-1"
}
terraform {
  backend "s3" {
    bucket = "demo-core-tfstate"
    key = "demo.tfstate"
    region = "eu-west-1"
  }
}
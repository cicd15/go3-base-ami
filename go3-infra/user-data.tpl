#!/bin/bash -xe
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

aws s3 cp s3://${artifact_bucket}/apps/demo/${env}/${app_version}/build.zip /home/ec2-user/growpy/build.zip

# get node into yum
curl --silent --location https://rpm.nodesource.com/setup_12.x | bash -
# install node (and npm) with yum
yum -y install nodejs
npm install -g serve
cd /home/ec2-user/growpy && unzip build.zip && rm -rf build.zip && serve -s build -l 80
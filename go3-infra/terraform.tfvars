############################################
###### module common ariables names ########
############################################
region                                = "eu-west-1"
name                                  = "demo"
adminAccessCidrBlock                  = ["176.139.115.140/32"]
environment                           = "prd"
stackName                             = "dla"
ansibleVersion                        = "2.8.2"
pythonVersion                         = "3"
nodeJsVersion                         = "12.8.1"
## This variable is a concataination of 2 vars:
# artifactsBucket = artifactsBucket_Name-stackName
artifactsBucket                       = "demo-core-artifacts-store-dla"
nvmVersion                            = "v0.35.3"
name_prefix                           = "demo"
env                                   = "prd"
app_version                           = "v1"
#main_zone                             = "growpy.info"
#main_zone_id                          = "Z04447463Q2EQVVO03307"
############################################
######## VPC module ariables values ########
############################################
create_vpc                            = true

cidr_block                            = "10.0.0.0/16"
azs                                   = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]

public_subnets                        = ["10.0.11.0/24", "10.0.12.0/24", "10.0.13.0/24"]
public_subnet_suffix                  = "public"
public_subnet_tags                    = {
  Team                                = "DevOps"
  Project                             = "appDemo"
  Environment                         = "dev"
  stack                               = "dla"
}

private_subnets                       = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
database_subnets                      = []
redshift_subnets                      = []
elasticache_subnets                   = []
intra_subnets                         = []

assign_generated_ipv6_cidr_block      = false

secondary_cidr_blocks                 = []

instance_tenancy                      = "default"


private_subnet_suffix                 = "private"
intra_subnet_suffix                   = "intra"
database_subnet_suffix                = "db"
redshift_subnet_suffix                = "redshift"
elasticache_subnet_suffix             = "elasticache"

create_database_subnet_route_table    = false
create_redshift_subnet_route_table    = false

enable_public_redshift                = false

create_elasticache_subnet_route_table = false

create_database_subnet_group          = false
create_elasticache_subnet_group       = false
create_redshift_subnet_group          = false

create_database_internet_gateway_route = false
create_database_nat_gateway_route     = false

enable_dns_hostnames                  = true

enable_dns_support                    = true

enable_nat_gateway                    = false
single_nat_gateway                    = false

one_nat_gateway_per_az                = false

reuse_nat_ips                         = false

external_nat_ip_ids                   = []

enable_dynamodb_endpoint              = false
enable_s3_endpoint                    = false
enable_sqs_endpoint                   = false

sqs_endpoint_security_group_ids       = []
sqs_endpoint_subnet_ids               = []
sqs_endpoint_private_dns_enabled      = false

enable_ssm_endpoint                   = false

ssm_endpoint_security_group_ids       = []
ssm_endpoint_subnet_ids               = []
ssm_endpoint_private_dns_enabled      = false

enable_ssmmessages_endpoint           = false

enable_apigw_endpoint                 = false

apigw_endpoint_security_group_ids     = []
apigw_endpoint_private_dns_enabled    = false
apigw_endpoint_subnet_ids             = []

ssmmessages_endpoint_security_group_ids = []
ssmmessages_endpoint_subnet_ids       = []
ssmmessages_endpoint_private_dns_enabled = false

enable_ec2_endpoint                   = false

ec2_endpoint_security_group_ids       = []
ec2_endpoint_private_dns_enabled      = false
ec2_endpoint_subnet_ids               = []
enable_ec2messages_endpoint           = false

ec2messages_endpoint_security_group_ids = []
ec2messages_endpoint_private_dns_enabled = false
ec2messages_endpoint_subnet_ids       = []

enable_ecr_api_endpoint               = false

ecr_api_endpoint_subnet_ids           = []

ecr_api_endpoint_private_dns_enabled  = false

ecr_api_endpoint_security_group_ids   = []

enable_ecr_dkr_endpoint               = false

ecr_dkr_endpoint_subnet_ids           = []

ecr_dkr_endpoint_private_dns_enabled  = false

ecr_dkr_endpoint_security_group_ids   = []

enable_kms_endpoint                   = false

kms_endpoint_security_group_ids       = []

kms_endpoint_subnet_ids               = []

kms_endpoint_private_dns_enabled      = false

enable_ecs_endpoint                   = false

ecs_endpoint_security_group_ids       = []

ecs_endpoint_subnet_ids               = []

ecs_endpoint_private_dns_enabled      = false

enable_ecs_agent_endpoint             = false

ecs_agent_endpoint_security_group_ids = []

ecs_agent_endpoint_subnet_ids         = []

ecs_agent_endpoint_private_dns_enabled = false

enable_ecs_telemetry_endpoint         = false

ecs_telemetry_endpoint_security_group_ids = []

ecs_telemetry_endpoint_subnet_ids     = []

ecs_telemetry_endpoint_private_dns_enabled = false

enable_sns_endpoint                   = false

sns_endpoint_security_group_ids       = []

sns_endpoint_subnet_ids               = []

sns_endpoint_private_dns_enabled      = false

enable_monitoring_endpoint            = false
monitoring_endpoint_security_group_ids = []
monitoring_endpoint_subnet_ids        = []
monitoring_endpoint_private_dns_enabled = false

enable_elasticloadbalancing_endpoint  = false
elasticloadbalancing_endpoint_security_group_ids = []
elasticloadbalancing_endpoint_subnet_ids = []
elasticloadbalancing_endpoint_private_dns_enabled = false

enable_events_endpoint                = false

events_endpoint_security_group_ids    = []

events_endpoint_subnet_ids            = []

events_endpoint_private_dns_enabled   = false

enable_logs_endpoint                  = false

logs_endpoint_security_group_ids      = []
logs_endpoint_subnet_ids              = []
logs_endpoint_private_dns_enabled     = false

enable_cloudtrail_endpoint            = false

cloudtrail_endpoint_security_group_ids = []
cloudtrail_endpoint_subnet_ids        = []
cloudtrail_endpoint_private_dns_enabled = false

map_public_ip_on_launch              = true

enable_vpn_gateway                   = false

vpn_gateway_id                      = ""

amazon_side_asn                     = "64512"

propagate_private_route_tables_vgw = false

propagate_public_route_tables_vgw = false

tags                              = {
  Environment                     = "dev"
  App                             = "app"
  Team                            = "DevOps"
  stack                           = "dla"
}
vpc_tags                          = {
  Environment                     = "dev"
  App                             = "app"
  Team                            = "DevOps"
  stack                           = "dla"
}

igw_tags                          = {}

private_subnet_tags               = {}

public_route_table_tags           = {}
private_route_table_tags          = {}
database_route_table_tags         = {}
redshift_route_table_tags         = {}
elasticache_route_table_tags      = {}
intra_route_table_tags            = {}

database_subnet_tags              = {}
database_subnet_group_tags        = {}

redshift_subnet_tags              = {}
redshift_subnet_group_tags        = {}

elasticache_subnet_tags           = {}
intra_subnet_tags                 = {}
public_acl_tags                   = {}
private_acl_tags                  = {}
intra_acl_tags                    = {}
database_acl_tags                 = {}
redshift_acl_tags                 = {}
elasticache_acl_tags              = {}
dhcp_options_tags                 = {}

nat_gateway_tags                  = {}
nat_eip_tags                      = {}

vpn_gateway_tags                  = {}

enable_dhcp_options               = false

dhcp_options_domain_name          = ""
dhcp_options_domain_name_servers  = ["AmazonProvidedDNS"]
dhcp_options_ntp_servers          = []
dhcp_options_netbios_name_servers = []
dhcp_options_netbios_node_type    = ""

manage_default_vpc                = true
default_vpc_name                  = "DO_NOT_USE"
default_vpc_enable_dns_support    = false
default_vpc_enable_dns_hostnames  = false
default_vpc_enable_classiclink    = false
default_vpc_tags                  = {
  Name = "DO_NOT_USE"
}

manage_default_network_acl     = false

default_network_acl_name     = ""

default_network_acl_tags     = {}

public_dedicated_network_acl     = false

private_dedicated_network_acl     = false

intra_dedicated_network_acl     = false

database_dedicated_network_acl     = false

redshift_dedicated_network_acl     = false

elasticache_dedicated_network_acl     = false

default_network_acl_ingress  = [
  {
    rule_no    = 100
    action     = "allow"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
    cidr_block = "0.0.0.0/0"
  },

  {
    rule_no         = 101
    action          = "allow"
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    ipv6_cidr_block = "::/0"
  },
]

default_network_acl_egress  = [
  {
    rule_no    = 100
    action     = "allow"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
    cidr_block = "0.0.0.0/0"
  },

  {
    rule_no         = 101
    action          = "allow"
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    ipv6_cidr_block = "::/0"
  },
]

public_inbound_acl_rules  = [
  {
    rule_number = 100
    rule_action = "allow"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_block  = "0.0.0.0/0"
  },
]
public_outbound_acl_rules  = [
  {
    rule_number = 100
    rule_action = "allow"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_block  = "0.0.0.0/0"
  },
]

private_inbound_acl_rules  = [
  {
    rule_number = 100
    rule_action = "allow"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_block  = "0.0.0.0/0"
  },
]
private_outbound_acl_rules  = [
  {
    rule_number = 100
    rule_action = "allow"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_block  = "0.0.0.0/0"
  },
]

intra_inbound_acl_rules  = [
  {
    rule_number = 100
    rule_action = "allow"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_block  = "0.0.0.0/0"
  },
]
intra_outbound_acl_rules  = [
  {
    rule_number = 100
    rule_action = "allow"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_block  = "0.0.0.0/0"
  },
]

database_inbound_acl_rules  = [
  {
    rule_number = 100
    rule_action = "allow"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_block  = "0.0.0.0/0"
  },
]
database_outbound_acl_rules  = [
  {
    rule_number = 100
    rule_action = "allow"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_block  = "0.0.0.0/0"
  },
]

redshift_inbound_acl_rules  = [
  {
    rule_number = 100
    rule_action = "allow"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_block  = "0.0.0.0/0"
  },
]
redshift_outbound_acl_rules  = [
  {
    rule_number = 100
    rule_action = "allow"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_block  = "0.0.0.0/0"
  },
]

elasticache_inbound_acl_rules  = [
  {
    rule_number = 100
    rule_action = "allow"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_block  = "0.0.0.0/0"
  },
]
elasticache_outbound_acl_rules  = [
  {
    rule_number = 100
    rule_action = "allow"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_block  = "0.0.0.0/0"
  },
]
############################################
######## LT module ariables values #########
############################################
create_lt                 = true

description               = "Launch Template for creating ASG"

image_id                  = "ami-099788906b236ecd1"
app_custom_ami_id         = "ami-099788906b236ecd1"
#app_custom_ami_id         = "ami-08435721d12527f9c"

instance_type             = "t2.micro"

key_name                  = "demoStackDevopsKp"

vpc_security_group_ids    = []

user_data                 = ""

iam_instance_profile_name = ""

iam_instance_profile_arn  = ""

device_name               = "/dev/xvdb"

volume_size               = "20"

volume_type               = "gp2"

delete_on_termination     = true

lt_tags                   = {
  Name                    = "appDemo"
  Team                    = "DevOps"
  Project                 = "appDemo"
  Environment             = "dev"
  stack                   = "dla"
}

#project_name              = "appDemo"
project_name              = "demo"

enabled_monitoring        = false

############################################
######## ASG module variables values #######
############################################
create_asg                = true

vpc_zone_identifier       = ["subnet-0fc763a53f49c5064", "subnet-05dd504f7bcd868c3", "subnet-01337b5fca4151d7a"]

min_size                  = "1"

desired_capacity          = "1"

max_size                  = "3"

health_check_grace_period = "300"

health_check_type         = "EC2"

force_delete              = false

#launch_template_id        = "lt-0a430c69269353851"
#launch_template_version   = "$Latest"

metrics_granularity       = "1Minute"

enabled_metrics           = [
  "GroupMinSize",
  "GroupMaxSize",
  "GroupDesiredCapacity",
  "GroupInServiceInstances",
  "GroupPendingInstances",
  "GroupStandbyInstances",
  "GroupTerminatingInstances",
  "GroupTotalInstances",
]
############################################
######## ALB module variables values #########
############################################
create_lb = true

enable_deletion_protection = false

enable_http2 = true

enable_cross_zone_load_balancing = false

extra_ssl_certs = []

https_listeners = []

http_tcp_listeners = []

idle_timeout = 60

ip_address_type = "ipv4"

listener_ssl_policy_default = "ELBSecurityPolicy-2016-08"

internal = false

load_balancer_create_timeout = "10m"

load_balancer_delete_timeout = "10m"

#name = null

load_balancer_type = "application"

load_balancer_update_timeout = "10m"

access_logs = {}

log_location_prefix = ""

subnets = null

subnet_mapping = []

#tags = {}

security_groups = []

target_groups = []

point_in_time_recovery_enabled = false

ttl_enabled = false

ttl_attribute_name = ""

autoscaling_defaults = {
  scale_in_cooldown  = 0
  scale_out_cooldown = 0
  target_value       = 70
}

autoscaling_read = {}

autoscaling_write = {}

autoscaling_indexes = {}

stream_enabled = false

stream_view_type = null

server_side_encryption_enabled = false

server_side_encryption_kms_key_arn = null

timeouts = {
  create = "10m"
  update = "60m"
  delete = "10m"
}

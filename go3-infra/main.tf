resource "aws_iam_role" "iamRole" {
  name                = "${var.env}-${var.name}-${var.stackName}IamRole"
  assume_role_policy  = file("instanceAssumeRolePolicies.json")
}

resource "aws_iam_policy" "iamPolicy" {
  name                = "${var.env}-${var.name}-${var.stackName}IamPolicy"
  description         = "Policies for the instance profile linked to the app server"
  policy              = file("instanceProfilePolicies.json")
}

resource "aws_iam_policy_attachment" "AppIamPolicyAttachment" {
  name = "${var.env}-${var.name}-${var.stackName}IamPolicyAttachment"
  roles = [aws_iam_role.iamRole.name]
  policy_arn = aws_iam_policy.iamPolicy.arn
}

resource "aws_iam_instance_profile" "AppInstanceProfile" {
  name = "${var.env}-${var.name}-${var.stackName}InstanceProfile"
  role = aws_iam_role.iamRole.name
}

resource "aws_security_group" "adminAccess" {
  name          = "${var.name}-${var.stackName}AdminAccess"
  description   = "SSH to App instances"
  vpc_id        = module.demo-vpc.vpc_id

  ingress {
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = var.adminAccessCidrBlock
  }

  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags          = {
    Name        = "${var.env}-${var.name}-${var.stackName}-AdminAccess"
    Environment = var.environment
    Project     = var.name
    Stack       = var.stackName
  }
}

resource "aws_security_group" "httpAccess" {
  name          = "${var.name}-${var.stackName}HttpAccess"
  description   = "Public access to App"
  vpc_id        = module.demo-vpc.vpc_id

  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    security_groups = [aws_security_group.albAccess.id]
  }

  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags          = {
    Name        = "${var.env}-${var.name}-${var.stackName}-HttpAccess"
    Environment = var.environment
    Project     = var.name
    Stack       = var.stackName
  }
}

resource "aws_security_group" "albAccess" {
  name          = "${var.name}-${var.stackName}-AlbAccess"
  description   = "Public access to demoApp"
  vpc_id        = module.demo-vpc.vpc_id

  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags          = {
    Name        = "${var.env}-${var.name}-${var.stackName}-AlbAccess"
    Environment = var.environment
    Project     = var.name
    Stack       = var.stackName
  }
}

data "template_file" "init" {
  template            = file("user-data.tpl")
  vars                = {
    env               = var.env
    app_version       = var.app_version
    artifact_bucket   = var.artifactsBucket
  }
}

resource "aws_launch_template" "this" {
  name 					= "${var.env}-${var.name}-${var.stackName}"
  description 			= var.description
  image_id 				= var.app_custom_ami_id
  instance_type 		= var.instance_type
  key_name 				= var.key_name
  vpc_security_group_ids = [
    aws_security_group.adminAccess.id, aws_security_group.httpAccess.id]
  user_data 			= base64encode(data.template_file.init.rendered)

  iam_instance_profile {
    name = aws_iam_instance_profile.AppInstanceProfile.name
  }

  ######################
  ## Taging resources ##
  ######################
  tags 					= var.lt_tags
  tag_specifications {
    resource_type 		= "instance"
    tags 				= {
      Name 			    = "${var.env}-${var.project_name}"
    }
  }
  tag_specifications {
    resource_type 		= "volume"
    tags 				= {
      Name 			    = "${var.env}-${var.project_name}"
    }
  }

  monitoring {
    enabled 			= var.enabled_monitoring
  }
}

module "demo-vpc" {
  source                = "./module/vpc"
  create_vpc            = var.create_vpc
  name                  = "${var.env}-${var.name}"
  cidr_block            = var.cidr_block
  azs                   = var.azs
  public_subnets        = var.public_subnets
  private_subnets       = var.private_subnets
  public_subnet_suffix  = var.public_subnet_suffix
  public_subnet_tags    = var.public_subnet_tags
  tags                  = var.tags

  manage_default_vpc    = var.manage_default_vpc
  default_vpc_name      = var.default_vpc_name
  default_vpc_tags      = var.default_vpc_tags

  enable_dns_hostnames  = var.enable_dns_hostnames
  enable_dns_support    = var.enable_dns_support
}

module "demo-asg" {
  source                = "./module/compute"
  create_asg            = var.create_asg
  name_prefix           = "${var.env}-${var.name_prefix}-${var.stackName}"
  vpc_zone_identifier   = module.demo-vpc.public_subnets

  min_size              = var.min_size
  desired_capacity      = var.desired_capacity
  max_size              = var.max_size

  health_check_grace_period = var.health_check_grace_period
  health_check_type     = var.health_check_type

  force_delete          = var.force_delete

  #launch_template_id    = aws_launch_template.this.id
  launch_template_name    = aws_launch_template.this.name
  launch_template_version = aws_launch_template.this.latest_version

  target_group_arns     = module.demo-alb.target_group_arns
}

module "demo-alb" {
  source                = "./module/alb"

  name                  = "${var.env}-${var.name}-${var.stackName}"
  load_balancer_type    = var.load_balancer_type

  vpc_id                = module.demo-vpc.vpc_id
  subnets               = module.demo-vpc.public_subnets
  security_groups       = [aws_security_group.albAccess.id]

  target_groups = [
    {
      name              = "${var.env}-${var.name_prefix}-${var.stackName}"
      backend_protocol  = "HTTP"
      backend_port      = 80
      target_type       = "instance"
    }
  ]

  http_tcp_listeners = [
    {
      port = 80
      protocol = "HTTP"
      target_group_index = 0
    }
  ]

}

#!/bin/bash
# Install node version manager
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
#Source bashrc
source $HOME/.bashrc
# Install a specific version of node
nvm install 12.8.1
# print versions of installed tools
nvm --version && npm --version
# Start the application
npm install -g serve
#Source bashrc
source $HOME/.bashrc